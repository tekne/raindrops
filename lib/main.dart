import 'package:flutter/material.dart';
import 'task.dart';

void main() {
  runApp(Raindrops());
}

class Raindrops extends StatelessWidget {
  @override
  build(context) {
    return MaterialApp(
        title: "raindrops",
        home: SafeArea(child: TaskList("Modlist")),
        theme: ThemeData(primaryColor: Colors.white));
  }
}

class TaskList extends StatefulWidget {
  final String name;
  TaskList(this.name);

  @override
  TaskListState createState() => TaskListState(name);
}

class TaskListState extends State<TaskList> {
  final String name;

  TaskListState(this.name);

  final _incompleteTasks = <Task>[
    Task("Install GregTech"),
    Task("Install IC2")
  ];

  Widget _buildTasks() {
    return ListView.separated(
      padding: const EdgeInsets.all(16.0),
      itemCount: _incompleteTasks.length,
      itemBuilder: (context, index) {
        return _buildTask(_incompleteTasks[index]);
      },
      separatorBuilder: (context, index) => const Divider(),
    );
  }

  void _openTask(Task task) {
    Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) => TaskDisplay(task)));
  }

  Widget _buildTask(Task task) {
    return ListTile(
      title: Text(task.name),
      onTap: () => _openTask(task),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: Text(name)), body: _buildTasks());
  }
}

class TaskDisplay extends StatelessWidget {
  final Task task;
  TaskDisplay(this.task);

  @override
  Widget build(BuildContext context) {
    final name = task.name;
    return Scaffold(appBar: AppBar(title: Text("$name")));
  }
}
